from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models


class User(AbstractUser, PermissionsMixin):
    pass
