from django.urls import path, include
from blog import urls as blog_urls

urlpatterns = [
    path('', include(blog_urls)),
]